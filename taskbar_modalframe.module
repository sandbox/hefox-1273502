<?php
/**
 * @file
 *  Provides a modalframe popup item for taskbar 
 */

/**
 * Implementation of hook_taskbar_provider().
 */
function taskbar_modalframe_taskbar_provider() {
  return array(
      array(
      'name' => 'modalframe',
      'title' => 'Modalframe (popup)',
      'ajax' => TRUE,
      'realtime' => FALSE,
      'callback' => 'taskbar_modalframe_callback',
      'settings form' => 'taskbar_modalframe_callback_settings_form',
      'settings form submit' => 'taskbar_modalframe_callback_settings_form_submit',
      'early prepare' => 'taskbar_modalframe_callback_settings_early_prepare'
    )
  );
}

/**
 * Implementation of hook_init().
 */
function taskbar_modalframe_init() {
  if (!empty($_GET['taskbar-modalframe'])) {
    modalframe_child_js();
    taskbar_suppress(TRUE);
  }
}

/**
 * Render callback for taskbar modalframe.
 */
function taskbar_modalframe_callback($item) {
  $prefix = 'taskbar-item-' . $item->name;
  $links = array();

  foreach ($item->settings['links'] as $delta => $link) {
    if (!isset($link['options']['attributes']['class'])) {
      $link['options']['attributes']['class'] = '';
    }

    if (empty($link['options']['query'])) {
      $link['options']['query'] = 'taskbar-modalframe=1';
    }
    else {
      if (is_array($link['options']['query'])) {
       $link['options']['query']['taskbar-modalframe'] = '1';
      }
      else {
       $link['options']['query'] .= '&taskbar-modalframe=1';
      }
    }

    $link['options']['attributes']['class'] .= ' taskbar-modalframe-child';
    $links[] = '<span class="taskbar-element ' . $prefix . ' ' . $prefix . '-' . $delta . '">' . l($link['title'], $link['url'], $link['options']) . '</span>';
  }

  return array('content' => implode('<span class="taskbar-item-separator"></span>', $links));
}

/**
 * Early Prepare callback for taskbar modalframe.
 */
function taskbar_modalframe_callback_settings_early_prepare($item) {
  modalframe_parent_js();
  drupal_add_js(drupal_get_path('module', 'taskbar_modalframe') . '/taskbar_modalframe_parent.js');
}

/**
 * Settings form for the taskbar modalframe provider.
 */
function taskbar_modalframe_callback_settings_form(&$form, &$form_state, $item) {
  $form['provider_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link settings'),
    '#collapsible' => TRUE,
  );
  
  if (empty($item->settings['links'])) {
    $item->settings['links'] = array();
  }
  $links = $item->settings['links'] + array('link' . (count($item->settings['links']) + 1) => array('title' => '', 'url' => ''));
  foreach ($links as $name => $link) {
    $form['provider_settings']['link_' . $name . '_title'] = array(
     '#type' => 'textfield',
     '#title' => t('Title'),
     '#default_value' => $link['title'],
     '#size' => 20,
    );
    $form['provider_settings']['link_' . $name . '_url'] = array(
     '#type' => 'textfield',
     '#title' => t('URL'),
     '#default_value' => $link['url'],
    );
  }
}


/**
 * Settings submit processing for the taskbar modalframe provider.
 */
function taskbar_modalframe_callback_settings_form_submit($form, &$form_state, &$item) {
  foreach ($form_state['values'] as $name => $value) {
    if (!preg_match('/^link_(.+?)_url/', $name, $matches)) {
      continue;
    }
    $name = $matches[1];
    if (empty($form_state['values']['link_' . $name . '_url'])) {
      if (isset($item->settings['links'][$name])) {
        unset($item->settings['links'][$name]);
      }
    }
    else {
      $item->settings['links'][$name] = array(
        'url' => $form_state['values']['link_' . $name . '_url'],
        'title' => $form_state['values']['link_' . $name . '_title'],
        'options' => array(),
      );
    }
  }
}
