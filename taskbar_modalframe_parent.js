// $Id: modalframe_example.js,v 1.1.2.7 2009/12/28 02:21:20 markuspetrux Exp $

(function ($) {

Drupal.behaviors.taskbarModalframe = function() {
  $('.taskbar-modalframe-child:not(.taskbar-modalframe-child-processed)').addClass('taskbar-modalframe-child-processed').click(function() {
    var element = this;

    // Build modal frame options.
    var modalOptions = {
      url: $(element).attr('href'),
      autoFit: true,
    };

    // Open the modal frame dialog.
    Drupal.modalFrame.open(modalOptions);

    // Prevent default action of the link click event.
    return false;
  });
};

})(jQuery);
